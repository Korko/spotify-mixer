<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use SpotifyWebAPI\Session;
use SpotifyWebAPI\SpotifyWebAPI;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->singleton(Session::class, function () {
            return new Session(
                config('spotify.client_id'),
                config('spotify.client_secret'),
                route('auth')
            );
        });

        $this->app->singleton(SpotifyWebAPI::class, function () {
            $options = [
                'auto_refresh' => true,
            ];

            return new SpotifyWebAPI($options, $this->app->make(Session::class));
        });
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array<int, string>
     */
    public function provides(): array
    {
        return [Session::class, SpotifyWebAPI::class];
    }
}
