<?php

namespace App\Http\Controllers;

use App\Service\SpotifyMixer;
use Facades\App\Service\ApiPaginator;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use SpotifyWebAPI\Session;
use SpotifyWebAPI\SpotifyWebAPI;
use SpotifyWebAPI\SpotifyWebAPIException;

class SpotifyController extends Controller
{
    public function __construct(Session $spotifySession, SpotifyWebAPI $spotifyApi)
    {
        $this->spotifySession = $spotifySession;
        $this->spotifyApi = $spotifyApi;
    }

    public function auth(Request $request) : RedirectResponse
    {
        if($request->has('code')) {
            $this->spotifySession->requestAccessToken($request->input('code'));

            session(['spotify-access-token' => $this->spotifySession->getAccessToken()]);
            session(['spotify-refresh-token' => $this->spotifySession->getRefreshToken()]);

            return redirect()->route('mix');
        } else {
            $options = [
                'scope' => [
                    'playlist-modify-private',
                    'playlist-modify-public',
                ],
            ];

            return redirect($this->spotifySession->getAuthorizeUrl($options));
        }
    }

    public function mix() : Response|RedirectResponse
    {
        try {
            return response()
                ->view('mix', [
                    'playlists' => ApiPaginator::all(fn($offset) => $this->spotifyApi->getMyPlaylists(['offset' => $offset]))
                ]);
        } catch (SpotifyWebAPIException $e) {
            return redirect()->route('auth');
        }
    }

    public function doMix(Request $request, SpotifyMixer $mixer) : JsonResponse|RedirectResponse
    {
        $validated = $request->validate([
            'playlists' => 'required|array|min:2',
            'playlists.*.rule' => 'required|in:size,min_duration,max_duration',
            'playlists.*.uri' => 'required|array|min:1',
            'playlists.*.uri.*' => 'required|string',
            'playlists.*.value' => 'required|integer|min:1',
        ]);

        try {
            foreach ($validated['playlists'] as $playlist) {
                $mixer->addPlaylist(
                    playlistId: $playlist['uri'],
                    length: ($playlist['rule'] === 'size' ? $playlist['value'] : null),
                    min_duration_ms: ($playlist['rule'] === 'min_duration' ? $playlist['value'] * 60 * 1000 : null),
                    max_duration_ms: ($playlist['rule'] === 'max_duration' ? $playlist['value'] * 60 * 1000 : null),
                );
            }

            $mix = $mixer->mix();

            return response()->json([
                'playlist_uri' => $mix->uri,
                'playlist_name' => $mix->name,
            ]);
        } catch (SpotifyWebAPIException $e) {
            return response()->json([
                'redirect' => route('auth')
            ]);
        }
    }
}
