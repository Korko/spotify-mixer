<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use SpotifyWebAPI\Session;
use Symfony\Component\HttpFoundation\Response;

class VerifySpotifyTokens
{
    public function __construct(Session $spotifySession)
    {
        $this->spotifySession = $spotifySession;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        if(session('spotify-access-token')) {
            $this->spotifySession->setAccessToken(session('spotify-access-token'));
        }

        if(session('spotify-refresh-token')) {
            $this->spotifySession->setRefreshToken(session('spotify-refresh-token'));
        }

        return $next($request);
    }
}
