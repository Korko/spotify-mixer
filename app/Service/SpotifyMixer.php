<?php

namespace App\Service;

use Facades\App\Service\ApiPaginator;
use Generator;
use SpotifyWebAPI\SpotifyWebAPI;

class SpotifyMixer
{
    protected $playlists;

    protected $spotifyApi;

    public function __construct(SpotifyWebAPI $spotifyApi)
    {
        $this->spotifyApi = $spotifyApi;
    }

    public function addPlaylist(string|array $playlistId, int $length = 1, int $min_duration_ms = null, int $max_duration_ms = null): self
    {
        $this->playlists[] = [
            'id' => $playlistId,
            'length' => $length,
            'min_duration_ms' => $min_duration_ms,
            'max_duration_ms' => $max_duration_ms,
        ];

        return $this;
    }

    public function mix(): object
    {
        // Create chunks of songs from each randomized playlist
        // Reduce the size of each playlist to a multiple of its length (to avoid having chunks of different sizes)
        $chunks = collect();
        foreach ($this->playlists as $playlist) {
            $chunks[] = collect($this->getPlaylistSongs($playlist['id']))
                ->shuffle()
                ->chunkWhile(function ($song, $key, $playlistChunks) use ($playlist) {
                    if(isset($playlist['min_duration_ms'])) {
                        // Add the song to the chunk if it's under the duration limit
                        return $playlistChunks->reduce(fn($sum, $song) => $sum + $song->track->duration_ms, 0) <= $playlist['min_duration_ms'];
                    } else if(isset($playlist['max_duration_ms'])) {
                        // Add the song to the chunk if it's under the duration limit
                        return $playlistChunks->reduce(fn($sum, $song) => $sum + $song->track->duration_ms, 0) + $song->track->duration_ms < $playlist['max_duration_ms'];
                    } else {
                        // Add the song to the chunk if it's under the length limit
                        return $playlistChunks->count() < $playlist['length'];
                    }
                })
                ->unless(isset($playlist['duration_ms']), function ($playlistChunks) use ($playlist) {
                    // If the last chunk does not respect the length, remove it
                    if ($playlistChunks->last()->count() < $playlist['length']) {
                        return $playlistChunks->splice(0, $playlistChunks->count() - 1);
                    }
                });
        }

        // Reduce each chunks to the same size
        $size = $chunks
            ->map(fn($chunk) => $chunk->count())
            ->min();

        // Combine chunks into a single songs playlist (zippering)
        $songs = $chunks
            ->map(fn($chunk) => $chunk->splice(0, $size))
            ->pipe(fn($chunks) => collect()->zip(...$chunks))
            ->flatten()
            ->whereNotNull()
            ->pluck('track.uri')
            ->toArray();

        return $this->createPlaylist('My new mix - ' . date('Y-m-d H:i:s'), $songs);
    }

    protected function getPlaylistSongs(string|array $playlistId): Generator
    {
        if (is_array($playlistId)) {
            foreach ($playlistId as $cursorPlaylistId) {
                yield from $this->getPlaylistSongs($cursorPlaylistId);
            }
        } else {
            yield from ApiPaginator::all(fn($offset) => $this->spotifyApi->getPlaylistTracks($playlistId, ['offset' => $offset]));
        }
    }

    protected function createPlaylist(string $name, array $songs): object
    {
        $mixPlaylist = $this->spotifyApi->createPlaylist([
            'name' => $name,
        ]);

        $songsChunks = array_chunk($songs, 100);
        foreach ($songsChunks as $songsChunk) {
            $this->spotifyApi->addPlaylistTracks($mixPlaylist->id, $songsChunk);
        }

        return $mixPlaylist;
    }
}
