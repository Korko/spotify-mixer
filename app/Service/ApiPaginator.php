<?php

namespace App\Service;

use Generator;

class ApiPaginator
{
    public function all(callable $apiCall): array
    {
        return iterator_to_array($this->yield($apiCall));
    }

    public function yield(callable $apiCall): Generator
    {
        $offset = 0;

        do {
            $answer = $apiCall($offset);

            foreach ($answer->items as $item) {
                yield $item;
            }

            $offset = $answer->offset + $answer->limit;
        } while ($answer->next);
    }
}
