<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Auth Application Client ID
    |--------------------------------------------------------------------------
    |
    | This value is the Client ID you can find in the Spotify for Developers
    | dashboard after creating a new application used to authenticate the
    | visitors of your website and fetch the data from the Spotify API.
    |
    */

    'client_id' => env('SPOTIFY_CLIENT_ID'),

    /*
    |--------------------------------------------------------------------------
    | Auth Application Client Secret
    |--------------------------------------------------------------------------
    |
    | This value is the Client Secret you can find next to the Client ID
    | in the Spotify for Developers dashboard for the application you
    | are using to auth visitors and fetch the playlists and songs.
    |
    */

    'client_secret' => env('SPOTIFY_CLIENT_SECRET'),

];
