/** @type {import('tailwindcss').Config} */
export default {
    content: [
        "./resources/**/*.blade.php",
        "./resources/**/*.js",
        "./node_modules/tw-elements/dist/js/**/*.js"
    ],
    darkMode: "class",
    plugins: [require("tw-elements/dist/plugin.cjs")],
};
