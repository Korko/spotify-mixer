import './bootstrap';

import { createApp, ref } from 'vue/dist/vue.esm-bundler.js';

var total_playlists = 0;
const default_playlist = { id: 1, uri: '', rule: 'size', value: 3 };

createApp({
    setup() {
        return {
            playlists: ref([]),
            mix_uri: ref(""),
            mix_name: ref(""),
        };
    },
    created() {
        // Can't mix with less than 2 playlists
        this.addPlaylist();
        this.addPlaylist();
    },
    methods: {
        addPlaylist() {
            this.playlists.push(Object.assign({}, default_playlist, { id: ++total_playlists }));
        },
        mixPlaylists() {
            if (window.confirm('Êtes-vous prêt à mélanger ces playlists ?')) {
                axios.post(this.$refs.form.dataset.target, { playlists: this.playlists })
                    .then((response) => {
                        if (response.redirect) {
                            window.location.href = response.redirect;
                        } else if (response.playlist_uri) {
                            this.mix_uri = response.playlist_uri;
                            this.mix_name = response.playlist_name;
                        }
                    })
            }
        },
        removePlaylist(index) {
            this.playlists.splice(index, 1);
        }
    }
}).mount('#app')
