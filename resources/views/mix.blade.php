<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Spotify Playlist Mixer</title>

        @vite('resources/css/app.css')
    </head>
    <body class="antialiased">
        <div class="relative sm:flex sm:justify-center sm:items-center min-h-screen bg-dots-darker bg-center bg-gray-100 dark:bg-dots-lighter dark:bg-gray-900 selection:bg-red-500 selection:text-white" id="app" v-cloak>
            <form data-target="{{ route('mix') }}" v-on:submit.prevent="mixPlaylists" ref="form">
                @csrf

                <div>
                    <div v-for="(playlist, index) in playlists" :key="playlist.id">
                        <select v-model="playlist.uri" multiple required>
                            @foreach ($playlists as $playlist)
                                <option value="{{  $playlist->uri }}">{{ $playlist->name }} ({{ $playlist->tracks->total }} titres)</option>
                            @endforeach
                        </select>
                        <select v-model="playlist.rule" required>
                            <option value="size">Quantité</option>
                            <option value="min_duration">Durée minimale (en minutes)</option>
                            <option value="max_duration">Durée maximale (en minutes)</option>
                        </select>
                        <input type="text" v-model="playlist.value" required />
                        <button
                          type="button"
                          class="inline-block rounded-full border-2 border-danger px-6 pb-[6px] pt-2 text-xs font-medium uppercase leading-normal text-danger disabled:text-inherit disabled:cursor-not-allowed disabled:border-neutral-500 transition duration-150 ease-in-out hover:border-danger-600 hover:bg-neutral-500 hover:bg-opacity-10 hover:text-danger-600 focus:border-danger-600 focus:text-danger-600 focus:outline-none focus:ring-0 active:border-danger-700 active:text-danger-700 dark:hover:bg-neutral-100 dark:hover:bg-opacity-10"
                          v-bind:disabled="playlists.length <= 2"
                          v-on:click.prevent="removePlaylist(index)">
                            Enlever la playlist
                        </button>
                    </div>

                    <button
                      type="button"
                      class="inline-block rounded-full border-2 border-primary-100 px-6 pb-[6px] pt-2 text-xs font-medium uppercase leading-normal text-primary-700 transition duration-150 ease-in-out hover:border-primary-accent-100 hover:bg-neutral-500 hover:bg-opacity-10 focus:border-primary-accent-100 focus:outline-none focus:ring-0 active:border-primary-accent-200 dark:text-primary-100 dark:hover:bg-neutral-100 dark:hover:bg-opacity-10"
                      v-on:click.prevent="addPlaylist">
                        Ajouter une playlist
                    </button>
                </div>

                <div class="text-center">
                    <button
                      type="submit"
                      class="inline-block rounded-full bg-primary px-6 pb-2 pt-2.5 text-xs font-medium uppercase leading-normal text-white shadow-[0_4px_9px_-4px_#3b71ca] transition duration-150 ease-in-out hover:bg-primary-600 hover:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] focus:bg-primary-600 focus:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] focus:outline-none focus:ring-0 active:bg-primary-700 active:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] dark:shadow-[0_4px_9px_-4px_rgba(59,113,202,0.5)] dark:hover:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.2),0_4px_18px_0_rgba(59,113,202,0.1)] dark:focus:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.2),0_4px_18px_0_rgba(59,113,202,0.1)] dark:active:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.2),0_4px_18px_0_rgba(59,113,202,0.1)]">
                        Mixer les playlists
                    </button>
                </div>
            </form>
            <div v-if="mix_uri"
              id="overlay"
              class="fixed z-40 w-screen h-screen inset-0 bg-gray-900 bg-opacity-60"
              tabindex="-1">
                <div id="dialog"
                  class="fixed z-50 top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2 w-96 bg-white rounded-md px-8 py-6 space-y-5 drop-shadow-lg flex flex-col text-center">
                    <div class="success-checkmark">
                        <div class="check-icon">
                        <span class="icon-line line-tip"></span>
                        <span class="icon-line line-long"></span>
                        <div class="icon-circle"></div>
                        <div class="icon-fix"></div>
                        </div>
                    </div>
                    <h3 class="font-bold">Playlist créée avec succès !</h3>
                    <p class="text-sm">Retrouvez votre mix dans la nouvelle playlist "@{{ mix_name }}" dans votre bibliothèque.</p>
                    <a
                        type="button"
                        class="inline-block rounded-full bg-primary px-6 pb-2 pt-2.5 text-xs font-medium uppercase leading-normal text-white shadow-[0_4px_9px_-4px_#3b71ca] transition duration-150 ease-in-out hover:bg-primary-600 hover:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] focus:bg-primary-600 focus:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] focus:outline-none focus:ring-0 active:bg-primary-700 active:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] dark:shadow-[0_4px_9px_-4px_rgba(59,113,202,0.5)] dark:hover:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.2),0_4px_18px_0_rgba(59,113,202,0.1)] dark:focus:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.2),0_4px_18px_0_rgba(59,113,202,0.1)] dark:active:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.2),0_4px_18px_0_rgba(59,113,202,0.1)]"
                        :href="mix_uri+':play'">
                        Lancer le mix
                    </a>
                </div>
            </div>
        </div>
        @vite('resources/js/app.js')
    </body>
</html>
